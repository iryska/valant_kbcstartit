function initSlider(tweets, insts, facebooks)
{
    var data = [];
    var i, j = 0, itw = 0, iin = 0, ifb = 0;
    for (i = 0; i < (
        tweets.length >= insts.length ?
            (tweets.length >= facebooks.length ?
                tweets.length :
                (facebooks.length >= insts.length ?
                    facebooks.length :
                    insts.length
                )
            ) : (facebooks.length >= insts.length ?
            facebooks.length :
            insts.length
        )); i++) {

        if (tweets[i]) {
            data.push(tweets[i]);
        } else if (tweets[itw]) {
            data.push(tweets[itw++]);
            if (!tweets[itw]) {
                itw = 0;
            }
        }
        if (insts[i]) {
            data.push(insts[i]);
        } else if (insts[iin]) {
            data.push(insts[iin++]);
            if (!insts[iin]) {
                iin = 0;
            }
        }
        if (facebooks[i]) {
            data.push(facebooks[i]);
        } else if (facebooks[ifb]) {
            data.push(facebooks[ifb++]);
            if (!facebooks[ifb]) {
                ifb = 0;
            }
        }
    }

    var $social = $('.social');
    var $post = $('.kbc-social ');
    var $footer = $('.footer');

    for (i = 0; i <= 100; ++i) {
        if(j > data.length - 1) {
            j = 0;
        }
        doScaledTimeout(i, j);
        j++;
    }

    function doScaledTimeout(i, j) {
        if (i == 0) {
            setTimeout(function() {
                showSlide(j, i);
            }, i * 8000);
        } else {
            setTimeout(function() {
                showSlide(j, i);
            }, 8000 + ((i - 1) * 6200));
        }
    }

    var arabic = /[\u0600-\u06FF]/;

    function showSlide(i, j) {
        $social.css({'display': ''});
        $social.find('.avatar').attr('src', data[i].avatar);
        $social.find('b').text(data[i].username);
        $social.find('.text span').html(data[i].text);
        $social.find('.account').find('b').text(data[i].account);
        $social.find('h3').text(data[i].fullName);
        $social.find('p').text('@' + data[i].username).css('margin-top', '-20px');

        if (arabic.test(data[i].text)) {
            $social.find('.text').addClass('rtl');
        } else {
            $social.find('.text').removeClass('rtl');
        }

        if(data[i].social == 'twitter') {
            $post.removeClass('instagram').removeClass('facebook').addClass('twitter');
            $footer.removeClass('instagram').removeClass('facebook').addClass('twitter');
            $footer.find('img').attr('src','images/twitter.jpg');
        } else if (data[i].social == 'instagram') {
            $post.removeClass('twitter').removeClass('facebook').addClass('instagram');
            $footer.removeClass('twitter').removeClass('facebook').addClass('instagram');
            $footer.find('img').attr('src','images/instagram.jpg');
        } else {
            $post.removeClass('instagram').removeClass('twitter').addClass('facebook');
            $footer.removeClass('instagram').removeClass('twitter').addClass('facebook');
            $footer.find('img').attr('src','images/facebook.png');
        }

        if(data[i].photo) {
            if (data[i].photo.indexOf(".mp4") > -1 || data[i].photo.indexOf(".avi") > -1 || data[i].photo.indexOf(".flv") > -1) {}
            else {
                $social.find('.photo').attr('src', data[i].photo);
                $social.find('.photo').css('display', 'block');
            }
            $social.find('.content').addClass('pushed');
        } else {
            $social.find('.photo').css('display', 'none');
            $social.find('.content').removeClass('pushed');
        }

        $('.jtextfill').textfill({
            maxFontPixels: 190,
            minFontPixels: 11
        });

        if (data[i + 1] && data[i + 1].photo) {
            preload(data[i + 1].photo);
        }
        if (data[i + 1] && data[i + 1].avatar) {
            preload(data[i + 1].avatar);
        }

        var delay = (j == 0 ? '2s' : '0.2s');

        $post.css('-webkit-animation-delay', delay);
        $post.css('animation-delay', delay);

        $post.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(e) {
            e.stopPropagation();
            $post.removeClass('animated rotateInUpLeftZoomIn');
            return hideSlide(i);
        });

        $post.addClass('animated rotateInUpLeftZoomIn');
        $footer.find('.follow').addClass('animated slideInUp');
        $footer.find('.fa').addClass('animated slideInUp');
        $footer.find('.account').addClass('animated slideInUp');
    }

    function hideSlide(i) {
        $footer.find('.follow').removeClass('animated slideInUp');
        $footer.find('.fa').removeClass('animated slideInUp');
        $footer.find('.account').removeClass('animated slideInUp');

        $social.addClass('animated zoomOut');
        $social.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(e) {
            e.stopPropagation();
            $social.css({'display': ''});
            $social.removeClass('animated zoomOut');
            return;
        });
    }
}

function preload(image) {
    if (image.indexOf(".mp4") > -1 || image.indexOf(".avi") > -1 || image.indexOf(".flv") > -1) {}
    else {
        (new Image()).src = image;
    }

}

function callProxy(url, success, params) {
    params = params || {};
    params.url = url;

    $.ajax({
        url: "proxy.php",
        type: 'post',
        data: params,
        success: success
    });
}

$(function() {
    var tweets = [];
    var tweetsUrl = 'http://data.seenspire.com/content_backend/feed_validation/social_demo/saved_feeds/tweets.xml';
    callProxy(tweetsUrl, function(xml) {
        var $doc = $(xml);
        $doc.find('tweet').each(function(i, el) {
            var $node = $(el);

            var avatar = $node.find('profile_image_url').text();
            var fullName = $node.find('name').text();
            var username = $node.find('screen_name').text();
            var photo = $node.find('media_url').text();
            var text = $node.find('text').text();
            var account = $node.find('account').text();

            var tweetsItem = {
                avatar: avatar,
                fullName: fullName,
                username: username,
                photo: photo,
                text: text,
                account: account,
                social: 'twitter'
            };
            tweets.push(tweetsItem);
        });
    });

    var insts = [];
    var instUrl = 'http://data.seenspire.com/content_backend/feed_validation/social_demo/saved_feeds/instagram.xml';
    callProxy(instUrl, function(xml) {
        var $doc = $(xml);
        $doc.find('item').each(function(i, el) {
            var $node = $(el);

            var avatar = $node.find('profile_picture').text();
            var fullName = $node.find('full_name').text();
            var username = $node.find('username').text();
            var photo = $node.find('media').text();
            var text = $node.find('text').text();
            var account = $node.find('account').text();

            var instItem = {
                avatar: avatar,
                fullName: fullName,
                username: username,
                photo: photo,
                text: text,
                account: account,
                social: 'instagram'
            };
            insts.push(instItem);
        });
    });

    var facebooks = [];
    var facebookUrl = 'http://data.seenspire.com/content_backend/feed_validation/social_demo/saved_feeds/facebook.xml';
    callProxy(facebookUrl, function(xml) {
        var $doc = $(xml);
        $doc.find('item').each(function(i, el) {
            var $node = $(el);

            var avatar = $node.find('profile_image').text();
            var fullName = $node.find('display_name').text();
            var username = $node.find('user_name').text();
            var photo = $node.find('image').text();
            var text = $node.find('text').text();
            var account = $node.find('account').text();

            var facebookItem = {
                avatar: avatar,
                fullName: fullName,
                username: username,
                photo: photo,
                text: text,
                account: account,
                social: 'facebook'
            };
            facebooks.push(facebookItem);
        });
    });

    setTimeout(function() {
        initSlider(tweets, insts, facebooks);
    }, 500);
});