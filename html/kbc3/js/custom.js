function callProxy(url, success, params) {
    params = params || {};
    params.url = url;

    $.ajax({
        url: "proxy.php",
        type: 'post',
        data: params,
        success: success
    });
}

function getNews() {
    var newsHost = 'http://upload.seenspire.com/sample_feed/Belga/int-national/';
    var newsUrl = newsHost + 'index.xml';

    callProxy(newsUrl, function(xml) {
        var $doc = $(xml);
        var news = [];
        $doc.find('NewsComponent').each(function(i, el) {
            if(i == 0) { return; }
            var $node = $(el);

            var title = $node.find('HeadLine').text();
            var ref = $node.find('NewsItemRef').attr('NewsItem');

            var newsItem = {
                title: title,
                ref: ref
            };
            news.push(newsItem);

            callProxy(newsHost + ref, function (refXml) {
                var $refDoc = $(refXml);

                var $imageRole = $refDoc.find('Role[FormalName=Image]');
                var img = $imageRole.siblings('ContentItem').attr('Href');

                if(img) {
                    newsItem.img = newsHost + img;
                    // preload images
                    newsItem.prelodedImg = new Image();
                    newsItem.prelodedImg.src = newsItem.img;
                }
            })
        });

        shuffle(news);
        setTimeout(function() {
            initSlider(news);
        },500);

    });
}

function initSlider(news) {
    var $promo = $('.page-promo');

    var $overlay1 = $('.dark-overlay');
    var $overlay2 = $('.light-overlay');
    var $overlays = $('.dark-overlay, .light-overlay');
    var $overlay = $('.overlay');

    var $newsWrap = $('.news');
    var $newsTitle = $('.news-title');

    // hide promo

    $promo.one('webkitAnimationStart mozAnimationStart  MSAnimationStart  oanimationstart  animationstart ', function(e) {
        // run loop: show overlay, show news item
        showOverlay(0);
    });
    $promo.addClass('animated fadeOut');

    function showNewsItem(i) {
        // don't show news item without image
        if(news[i].img == undefined) {
            if(i < news.length - 1) {
                showNewsItem(++i);
            } else {
                showNewsItem(0);
            }
            return;
        }

        console.log(news[i]);
        $newsWrap.css('background-image', 'url(' + news[i].img + ')');
        $newsTitle.text(news[i].title);

        $newsWrap.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            console.log('showed newsItem: ' + i);
            if(i < news.length - 1) {
                showOverlay(++i);
            } else {
                showOverlay(0);
            }
            $newsWrap.removeClass('animated zoomOut1_2');
        });
        $newsWrap.addClass('animated zoomOut1_2');
    }

    function showOverlay(i) {

        var delay = (i == 0) ? '0' : '1';

        $overlay.css('-webkit-animation-delay', delay+'s');
        $overlay.css('animation-delay', delay+'s');

        $overlay.one('webkitAnimationStart mozAnimationStart  MSAnimationStart  oanimationstart  animationstart ', function(e) {
            console.log('start overlay: ' + i);
            showNewsItem(i);
        });
        $overlay.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            console.log('showed overlay: ' + i);
            $overlay.removeClass('animated slideInOutRight');
        });


        $overlay.addClass('animated slideInOutRight');
        /*
        $overlay1.css('-webkit-animation-delay', delay+'s');
        $overlay1.css('animation-delay', delay+'s');
        $overlay2.css('-webkit-animation-delay', delay+'.2s');
        $overlay2.css('animation-delay', delay+'.2s');

        $overlay2.one('webkitAnimationStart mozAnimationStart  MSAnimationStart  oanimationstart  animationstart ', function(e) {
            showNewsItem(i);
        });

        $overlay2.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            console.log('showed overlay: ' + i);
            $overlays.removeClass('animated slideInOutRight');
            //showNewsItem(i);
        });
        $overlays.addClass('animated slideInOutRight');
        */
    }
}

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex ;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}


$(function() {
   getNews();
});