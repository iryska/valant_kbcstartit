var Countdown = function(dateStr, options) {
    var eventDate = new Date(dateStr),
        $timer = $('.kbc-countdown .timer'),
        offset,
        clock,
        interval;

    // default options
    options = options || {};
    options.delay = options.delay || 100;

    function init() {
        $('.kbc-subtitle .month').text(eventDate.format('mmmm'));
        $('.kbc-subtitle .day').text(eventDate.format('dS'));

        reset();

        return this;
    }

    function reset() {
        clock = eventDate.getTime() - Date.now();
        render();
    }

    function start() {
        if (!interval) {
            if(eventDate > (new Date())) {
                offset = Date.now();
                interval = setInterval(update, options.delay);
            } else {
                clock = 0;
                render();
            }
        }
    }

    function stop() {
        if (interval) {
            clearInterval(interval);
            interval = null;
        }
    }

    function getDelta() {
        var now = Date.now(),
            d   = now - offset;

        offset = now;
        return d;
    }

    function update() {
        clock -= getDelta();
        render();
    }

    function render() {
        var d = parseInt((clock/1000) / (60 * 60 * 24));
        var h = parseInt((clock/1000) / (60 * 60)) % 24;
        var m = parseInt( clock/1000 / 60 ) % 60;
        var s = parseInt( clock/1000 ) % 60;

        $timer.find('.seconds .digit').text(s < 10 ? '0' + s : s);
        $timer.find('.minutes .digit').text(m < 10 ? '0' + m : m);
        $timer.find('.hours .digit').text(h < 10 ? '0' + h : h);
        $timer.find('.days .digit').text(d);
    }

    return {
        init: init,
        start: start
    };
};

function animate($element, animation) {
    $element.addClass(animation + ' animated');
    return $element;
}

$(function() {
    var cd = new Countdown('Jun 30 2015', { delay: 100});
    cd.init().start();
});