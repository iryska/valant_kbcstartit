
$.fn.random = function() {
    return this.eq(Math.floor(Math.random() * this.length));
};

$(document).ready(function(){
    setTimeout(function(){
        initSlider();
    }, 5000);
});

function initSlider() {
    var $popup = $('.popup').removeClass('active').random();

    var curentPos = $popup.position();

    $popup.css('top', curentPos.top + 'px ').css('left', curentPos.left).css('position', 'absolute');

    setTimeout(function() {
        var width = $(window).width();
        var height = $(window).height();
        var left = width * 0.35 / 2;
        var top = (height - 500) / 2;

        $('.overlay').fadeIn();

        $popup.css('-webkit-transition-duration', '2s');
        $popup.css('top', top + 'px').css('left', left + 'px');
        $popup.addClass('active');

        $popup.find('.content').css('-webkit-animation-delay', '1.5s').css('animation-delay', '1.5s');
        $popup.find('.content').addClass('slideInRight');

        $popup.find('.content').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            var $content = $(this);

            $content.find('.web').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                $content.css('-webkit-animation-delay', '5s').css('animation-delay', '5s');
                $content.removeClass('slideInRight').addClass('slideOutLeft');

                setTimeout(function() {
                    $content.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function (e) {
                        $('.overlay').fadeOut();
                        $popup.css('top', curentPos.top + 'px ').css('left', curentPos.left);
                        initSlider();
                    });
                }, 1000);
            });

        });
    });

}