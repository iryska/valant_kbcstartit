$.fn.random = function() {
    return this.eq(Math.floor(Math.random() * this.length));
};

$(document).ready(function(){
    setTimeout(function(){
        initSlider();
    }, 4000);
});

function initSlider() {
    var $popup = $('.popup').removeClass('active').random();
    $popup.addClass('active');

    $popup.find('.content').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        setTimeout(function(){
            initSlider()
        }, 5000);
    });
}