function initCounters() {
    var data = {
        startups: 134,
        people: 331,
        mentors: 34,
        events: 121,
        coffees: 14006
    };

    $counter = $('.countdown');
    var interval;
    var state = {
        startups: 0,
        people: 0,
        mentors: 0,
        events: 0,
        coffees: 0
    };


    function countUp() {
        var end = true;

        if(state.startups <= data.startups) {
            $counter.find('.startups .number').text(state.startups);
            state.startups++;
            end = false;
        }
        if(state.people <= data.people) {
            $counter.find('.people .number').text(state.people);
            state.people++;
            end = false;
        }
        if(state.mentors <= data.mentors) {
            $counter.find('.mentors .number').text(state.mentors);
            state.mentors++;
            end = false;
        }
        if(state.events <= data.events) {
            $counter.find('.events .number').text(state.events);
            state.events++;
            end = false;
        }
        if(state.coffees <= data.coffees) {
            $counter.find('.coffees .number').text(state.coffees);
            state.coffees += 50;
            end = false;
        } else {
            $counter.find('.coffees .number').text(data.coffees);
        }

        if(end) {
            clearInterval(interval);
            initTextSlider();
        }
    }

    interval = setInterval(countUp, 10);
}

function initTextSlider() {
    $('.text1').addClass('animated fadeOut');
    $('.text1').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        //showText(2)
        $('.text1').css('display', 'none');
        $('#my-slideshow').addClass('animated fadeIn');
        $('#my-slideshow').bjqs({
            'height' : 385,
            'width' : 1500,
            'animspeed': 2000,
            'responsive' : true,
            'showcontrols' : false,
            'centercontrols' : false,
            'showmarkers' : false,
            'centermarkers' : false,
            'keyboardnav' : false,
            'hoverpause' : false,
            'usecaptions' : false,
            'randomstart' : false
        });
    });
}

$(function(){
    $('.item1 .title').one('webkitAnimationStart mozAnimationStart MSAnimationStart oanimationstart animationstart', initCounters);
});