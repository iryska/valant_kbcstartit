function Sizes(){
    jQuery('.event .right-shadow').width(width / 2 + 'px');
    jQuery('.event-title').css('font-size', width / 22.588 + 'px').css('padding-left', width / 34.9 + 'px');
    jQuery('.event-date').css('font-size', width / 27.439 + 'px').css('padding-left', width / 34.9 + 'px');
    //jQuery('.kbc-title h1').css('font-size', width / 17.715 + 'px');
    jQuery('.kbc-logo img').css('width', width / 4 + 'px');
    var event_height = parseInt(jQuery('.event-body').outerHeight());
    var shadow_height = parseInt(jQuery('.event .bottom-shadow').outerHeight());
    jQuery('body').css('min-height', event_height + shadow_height + jQuery('#kbc-wrap').outerHeight() + 'px');
    jQuery('.event .bottom-shadow').css('bottom', -shadow_height + 'px');
}

function initSlider() {
    var $event = $('#event');
    var data = [
        {
            image: 'http://placehold.it/603x450/81D4FA?1',
            date: '30 January | 20:00 - 21:00',
            title: 'From Predictive maintenance to alzheimer slippers (keynote Ritchie Houtmeyers)'
        },
        {
            image: 'http://placehold.it/603x450/E6EE9C?2',
            date: '11 February | 12:30 - 13:30',
            title: 'Keynote by Xavier Damman (co-founder Storify): The digital marketeer is the new curator'
        },
        {
            image: 'http://placehold.it/603x450/EF9A9A?3',
            date: '12 February | 17:00 - 19:00',
            title: 'From Predictive maintenance to alzheimer slippers (keynote Ritchie Houtmeyers)'
        }
    ];

    function showSlide(i) {
        if(i > data.length - 1) {
            console.log('loop');
            i = 0;
        }

        $event.find('.event-photo').attr('src', data[i].image).load(Sizes);
        $event.find('.event-date').text(data[i].date);
        $event.find('.event-title').text(data[i].title );

        var delay = (i == 0 ? '0.7s' : '0s');

        $event.css('-webkit-animation-delay', delay);
        $event.css('animation-delay', delay);

        $event.addClass('animated slideInUpLeft');

        $event.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            console.log('showed: ' + i);
            $event.removeClass('animated slideInUpLeft');

            hideSlide(i);
        });
    }

    function hideSlide(i) {
        $event.css('-webkit-animation-delay', '5s');
        $event.css('animation-delay', '5s');

        $event.addClass('animated slideOutDownRight');

        $event.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            console.log('hidden: ' + i);
            $event.removeClass('animated slideOutDownRight');
            showSlide(++i);
        });
    }

    // start sliding
    showSlide(0);
}

jQuery(document).ready(function(){
    initSlider();
    Sizes();
});
// Window Resized
var width = jQuery(window).width();
var height = jQuery(window).height();
jQuery(window).resize(function ($) {
    if (jQuery(this).width() != width) {
        width = jQuery(window).width();
        // some function there
        Sizes();
    }
    if (jQuery(this).height() != height) {
        height = jQuery(window).height();
        // some function there
        Sizes();
    }
});